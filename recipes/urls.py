from django.urls import path, include
from recipes.views import recipe_list, show_recipe, create_recipe, edit_recipe, redirect_to_recipe_list, my_recipe_list
from django.shortcuts import redirect
from accounts.views import signup, user_login, user_logout


urlpatterns = [
    path("", recipe_list, name="recipe_list"),
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
    path("", redirect_to_recipe_list, name="home_page"),
    path("accounts/signup/", signup, name="signup"),
    path("login/", user_login, name="user_login"),
    path("logout/", user_logout, name="user_logout"),
    path("mine/", my_recipe_list, name="my_recipe_list"),

]
